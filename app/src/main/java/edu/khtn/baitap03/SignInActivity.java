package edu.khtn.baitap03;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.provider.AlarmClock;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class SignInActivity extends Activity {
    public final static String EXTRA_MESSAGE = "edu.khtn.baitap03.MESSAGE";
    TextView edtName, edtPass;
    Button btnSignIn;
    String[] userOnFile;
    String name, pass;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin);
        edtName = (TextView) findViewById(R.id.edtName);
        edtPass = (TextView) findViewById(R.id.edtPass);
        btnSignIn = (Button) findViewById(R.id.btnSignIn);
        btnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userOnFile = docFile();
                name = String.valueOf(edtName.getText());
                pass = String.valueOf(edtPass.getText());
                if (name.contentEquals(userOnFile[0]) && pass.contentEquals(userOnFile[1])) {
                    finish();
                    openTrangChu(name);
                } else {
                    thongBao("Tên hoặc password chưa đúng",false);
                }
            }
        });
    }

    public String[] docFile(){
        try {
            FileInputStream input = openFileInput("UserFile");
            String doc = "";
            int count = input.available();
            for (int i = 0; i < count; i++) {
                doc += (char) input.read();
            }
            String[] user = doc.split("-");
            input.close();
            return user;
        } catch (FileNotFoundException e) {
            thongBao("Không tìm thấy file",false);
            return null;
        } catch (IOException e) {
            thongBao("Không thể đọc file",false);
            return null;
        }
    }

    public void thongBao(String noiDung, Boolean exitOrNot){
        AlertDialog.Builder msg = new AlertDialog.Builder(this);
        msg.setTitle("Thông báo!");
        if (exitOrNot.booleanValue()) {
            msg.setMessage(noiDung);
            msg.setPositiveButton("Exit", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            });
        }else {
            msg.setMessage(noiDung);
            msg.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
        }
        msg.show();
    }

    public void openTrangChu(String message){
        Intent intent = new Intent(this,MainActivity.class);
        intent.putExtra(EXTRA_MESSAGE,message);
        startActivity(intent);
    }
}
