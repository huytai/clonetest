﻿package edu.khtn.baitap03;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class InOutFile extends Activity{

    public void ghiFile(String s){
        try {
            FileOutputStream output = openFileOutput("InOutStream", MODE_PRIVATE);
            char[] chars = s.toCharArray();
            for (int i = 0; i < chars.length; i++) {
                output.write(chars[i]);
            }
            output.close();
        } catch (FileNotFoundException e) {
            thongBao("Không tìm thấy file", false);
        } catch (IOException e) {
            thongBao("Không thể ghi file", false);
        }
    }

    public void writeFile(String s){
        try {
            FileWriter wtr = new FileWriter(new File("ReadWrite"));
            wtr.write(s);
            wtr.close();
        } catch (IOException e) {
            thongBao("Không thể write file",false);
        }
    }

    public void printFile(String s){
        try {
            PrintWriter printWtr = new PrintWriter(new File("PrintScan"));
            printWtr.println(s);
            printWtr.close();
        } catch (FileNotFoundException e) {
            thongBao("Không tìm thấy file",false);
        }
    }

    public String[] readFile(){
        try {
            FileReader rdr = new FileReader(new File("ReadWrite"));
            String doc = String.valueOf(rdr.read());
            thongBao("ReadWrite: " + doc, false);
            String[] user = doc.split("-");
            rdr.close();
            return user;
        } catch (FileNotFoundException e) {
            thongBao("Không tìm thấy file",false);
            return null;
        } catch (IOException e) {
            thongBao("Không thể đọc file",false);
            return null;
        }
    }

    public String[] scanFile(){
        try {
            Scanner scan = new Scanner(new File("PrintScan"));
            String doc = scan.nextLine();
            thongBao("PrintScan: " + doc, false);
            String[] user = doc.split(",");
            scan.close();
            return user;
        } catch (FileNotFoundException e) {
            thongBao("Không tìm thấy file",false);
            return null;
        }
    }

    public void thongBao(String noiDung, Boolean exitOrNot){
        AlertDialog.Builder msg = new AlertDialog.Builder(this);
        msg.setTitle("Thông báo!");
        if (exitOrNot.booleanValue()) {
            msg.setMessage(noiDung);
            msg.setPositiveButton("Exit", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            });
        }else {
            msg.setMessage(noiDung);
            msg.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
        }
        msg.show();
    }

}
